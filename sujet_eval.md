# Évaluation : Docker, Intégration continue, sécurité

## Partie 1 : mettre en place l'intégration continue d'un petit projet java

Le projet suivant : [Benoit Averty / simple-http-hello-world · GitLab](https://gitlab.com/BenoitAverty/simple-http-hello-world) est un projet java simple et autonome (pas de base de données, un jar exécutable)

1. Mettre en place la conteneurisation de ce projet et rédiger la documentation qui permet de lancer le projet avec Docker. Cette doc doit être une section dans le README qui sera suivie pas à pas pour tester qu'on arrive bien à lancer le projet avec Docker.
2. Mettre en place une pipeline d'intégration continue avec Gitlab CI pour ce projet. Pour tester que cette pipeline fonctionne, on regardera dans votre projet Gitlab si la pipeline s'est exécutée correctement et si une image docker est disponible dans le registry gitlab.

## Partie 2 : Sécurité

L'injection SQL est une faille de sécurité très courante et relativement simple à exploiter. Il est également assez simple de s'en protéger si on suit quelques règles lors du développement d'applications avec base de donnée SQL.

Pour bien comprendre cette faille, le mieux reste de l'exploiter soi-même.

1. Facultatif mais recommandé : suivre l''exercice d'injection SQL à l'adresse suivante : [SQL Injection](https://www.hacksplaining.com/exercises/sql-injection)
2. Créer un compte sur le site [Root Me](https://www.root-me.org) puis rendez-vous sur [Cette page vulnérable à l'injection SQL](http://challenge01.root-me.org/web-serveur/ch9/). Votre rôle est de trouver le mot de passe de l'administrateur (envoyez-le par message privé sur slack).

Pour cette partie, il peut être utile de rendre visible le contenu d'un champ caché grace aux dev-tools de votre navigateur (demandez de l'aide pour cette partie si nécéssaire.)

## Partie 3 (bonus) : Docker compose

Développer un fichier docker compose qui permet de démarrer votre projet de gestionnaire de quiz en local. Le fichier devra démarrer plusieurs containers : 
 * L'application Java
 * La base de données postgres
 * (bonus dans le bonus) : l'application front

Les trois containers doivent communiquer entre eux pour que l'application fonctionne sans autre dépendance.
